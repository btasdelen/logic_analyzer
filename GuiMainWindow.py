import sys
import os
from PlotContainer import PlotContainer
from PyQt5 import QtCore, QtGui, QtWidgets
from SerialCommPanel import *


class Communicate(QtCore.QObject):

    update_plot = QtCore.pyqtSignal(int)


class MyRectangle(QtWidgets.QWidget):

  def __init__(self, color=[255, 0, 0]):
    super(MyRectangle, self).__init__()
    self.color = color
    self.setFixedWidth(150)

  def paintEvent(self, qpe):

    qp = QtGui.QPainter()
    qp.begin(self)
    self.drawWidget(qp)
    qp.end()

  def drawWidget(self, qp):

    qp.setPen(QtGui.QColor(self.color[0], self.color[1], self.color[2]))
    qp.setBrush(QtGui.QColor(self.color[0], self.color[1], self.color[2]))
    ss = self.size()
    qp.drawRect(0, 0, ss.width(), ss.height())

  def setColor(self, r, g, b):
    self.color = [r, g, b]

class HScrollArea(QtWidgets.QScrollArea):
    def wheelEvent(self, e):
      if e.modifiers() and QtCore.Qt.ControlModifier:
        fake_event = QtGui.QWheelEvent(e.posF(), e.globalPosF(), e.pixelDelta(),
                                   e.angleDelta(), e.angleDelta().y(), QtCore.Qt.Horizontal,
                                   e.buttons(), e.modifiers())
        super(HScrollArea, self).wheelEvent(fake_event)


class MainWindow(QtWidgets.QWidget):

  def __init__(self):
    super(MainWindow, self).__init__()

    hbox = QtWidgets.QHBoxLayout()
    splitter = QtWidgets.QSplitter()

    self.left_panel = LeftPanel(self)
    self.plot_container = PlotContainer()

    # Signal
    self.freq = 1e6
    self.sample_time = 1

    # self.plot_container.addChannel([1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0])
    self.plot_container.setFreq(self.freq)

    self.scroll_area = HScrollArea()
    self.scroll_area.setWidget(self.plot_container)

    rect2 = MyRectangle([100, 255, 0])

    style_str = ""

    with open(os.path.join("style", "style.css"), "r") as st:
        style_str = st.read()
    self.setStyleSheet(style_str)
    splitter.addWidget(self.left_panel)
    splitter.addWidget(self.scroll_area)
    splitter.addWidget(rect2)

    # hbox.addWidget(self.left_panel)
    # hbox.addWidget(self.scroll_area)
    # hbox.addWidget(rect2)

    self.setLayout(hbox)
    hbox.addWidget(splitter)
    self.setWindowTitle('Lojik Analiyzir')
    self.setGeometry(50, 50, 640, 480)
    self.setMinimumSize(320, 240)

    self.c = Communicate()
    self.c.update_plot[int].connect(self.scroll_area.wheelEvent)


  def resizeEvent(self, e):
    super(MainWindow, self).resizeEvent(e)
    self.plot_container.calculateZoom()

  def setFreq(self):

    print(self.left_panel.serial_panel.options_box.freq_line.text())
    try:
        int(self.left_panel.serial_panel.options_box.freq_line.text())
    except Exception:
        print('Error Input can only be a number')
        self.left_panel.serial_panel.options_box.freq_line.setText(str(self.freq))

    self.freq = int(self.left_panel.serial_panel.options_box.freq_line.text())

    self.plot_container.setFreq(self.freq)
    self.plot_container.update()



class LeftPanel(QtWidgets.QWidget):

  def __init__(self, p):
    super(LeftPanel, self).__init__()

    self.parent = p

    vbox = QtWidgets.QVBoxLayout()
    self.setFixedWidth(200)
    self.serial_panel = SerialCommPanel(self)
    rect2 = MyRectangle([0, 1, 200])

    vbox.addWidget(self.serial_panel)
    vbox.addWidget(rect2)
    self.setLayout(vbox)


def main():

  app = QtWidgets.QApplication(sys.argv)
  main_window = MainWindow()
  main_window.show()
  sys.exit(app.exec_())


if __name__ == '__main__':
    main()
