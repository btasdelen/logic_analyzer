from enum import Enum, unique


@unique
class Cmd(Enum):
    START_CMD = b"\x01"
    GET_MAX_CH_NUM = b"\x02"
    GET_MAX_SAMP_RATE = b"\x03"
    GET_AVAIL_MEM_SIZE = b"\x04"
    SET_SAMP_RATE = b"\x0A"
    SET_SAMP_DURATION = b"\x0B"
    SEND_ERR = b"\x7E"
    START_SAMP = b"\xFE"
    END_SAMP = b"\xFF"

    def list_available_cmds():
        cmd_list = [str(name)[4:] for name in Cmd]
        return "\t".join(cmd_list)

# print(Cmd.list_available_cmds())
