##
# Change colors !!!
##

from PyQt5 import QtCore, QtGui, QtWidgets
from SignalPlotter import SignalPlotter
from TimeAxis import TimeAxis


class PlotContainer(QtWidgets.QWidget):

    def __init__(self):
        super(PlotContainer, self).__init__()

        # Layout
        self.plot_area_layout = QtWidgets.QVBoxLayout()
        self.plot_area_layout.addStretch(1)
        self.setLayout(self.plot_area_layout)

        self.zoom = 50
        self.length = 30

        # Time axis
        self.freq = 10000
        self.time_axis = TimeAxis()
        self.plot_area_layout.addWidget(self.time_axis)

        self.num_channel = 0
        self.plots = []

        self.back_color = QtCore.Qt.white
        pal = QtGui.QPalette()
        pal.setColor(QtGui.QPalette.Background, self.back_color)
        self.setAutoFillBackground(True)
        self.setPalette(pal)

    def wheelEvent(self, e):
        if not e.modifiers():
            num_degree = e.angleDelta()

            if not num_degree.isNull():
                if num_degree.y() < 0:
                    self.zoom /= 1.5
                else:
                    self.zoom *= 1.5

        self.calculateZoom()
        e.accept()

    def calculateZoom(self):

        parent_size = self.parentWidget().width() - 20

        if self.zoom < parent_size / self.length:
            self.zoom = parent_size / self.length
        for i in range(0, self.num_channel):
            self.plots[i].setZoom(self.zoom)

        self.time_axis.setZoom(self.zoom)

        self.setFixedSize(self.length * self.zoom + 20,
                          70 * len(self.plots) + 70)
        self.update()

    def addChannel(self, s):

        self.num_channel += 1
        self.plots.append(SignalPlotter(s, self.freq))

        self.plots[-1].setColor(QtCore.Qt.darkGray, QtCore.Qt.white)

        self.plot_area_layout.addWidget(self.plots[-1])
        self.length = len(s)

        self.time_axis.changeLen(self.length)
        self.calculateZoom()

    def setFreq(self, f):
        self.time_axis.setFreq(f)
        for i in range(0, self.num_channel):
            self.plots[i].setFreq(f)
        self.freq = f
