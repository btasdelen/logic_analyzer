import os
from PyQt5 import QtCore, QtGui, QtWidgets
from serial.tools import list_ports
import time
from threading import Thread
from SerialComm import SerialComm
import random

with open(os.path.join("style", "connected.css"), "r") as fh:
    connected_style = fh.read()

with open(os.path.join("style", "not_connected.css"), "r") as fh:
    not_connected_style = fh.read()


class OptionBox(QtWidgets.QWidget):
    def __init__(self, p):
        super(OptionBox, self).__init__()
        self.is_visible = False
        self.parent = p

        self.vbox = QtWidgets.QVBoxLayout()

        self.freq_line = QtWidgets.QLineEdit()
        self.freq_line.setMinimumWidth(125)

        self.change_freq_button = QtWidgets.QPushButton("Change Freq")
        self.change_freq_button.clicked.connect(
        self.parent.parent.parent.setFreq)

        self.back_color = QtCore.Qt.white
        pal = QtGui.QPalette()
        pal.setColor(QtGui.QPalette.Background, self.back_color)
        self.setAutoFillBackground(True)
        self.setPalette(pal)

        self.vbox.addWidget(self.freq_line)
        self.vbox.addWidget(self.change_freq_button)

        self.setLayout(self.vbox)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.Popup)

    def showHide(self):
        # Apply, Close
        if self.is_visible:
            self.hide()
            self.parent.options_button.setText("Options")
            self.is_visible = False
            self.parent.parent.parent.setFreq()

            print(self.parent.parent.parent.freq)

        # Open
        else:
            k = self.parent.mapToGlobal(self.parent.pos())
            self.setGeometry(k.x() + 150, k.y() + 100, 100, 100)
            self.show()
            self.parent.options_button.setText("Apply")
            self.is_visible = True


class SerialCommPanel(QtWidgets.QWidget):

    def __init__(self, p):
        super(SerialCommPanel, self).__init__()
        self.connection = SerialComm()

        hbox = QtWidgets.QHBoxLayout()
        vbox = QtWidgets.QVBoxLayout()

        self.parent = p

        self.sample_button = QtWidgets.QPushButton()
        self.sample_button.setText("Simulate")
        self.sample_button.setObjectName("sample_button")
        self.sample_button.setFixedSize(140, 40)

        self.options_button = QtWidgets.QPushButton()
        self.options_button.setText("↕")
        self.options_button.setObjectName("options_button")
        self.options_button.setFixedSize(40, 40)
        vbox.setSpacing(0)
        hbox.addStretch(1)

        self.options_box = OptionBox(self)

        hbox.addWidget(self.sample_button)
        hbox.addWidget(self.options_button)
        hbox.addStretch(1)

        # Define Connetcion Notification Label
        self.connection_indicator = QtWidgets.QLabel()
        self.connection_indicator.setText("Not Connected")
        self.connection_indicator.setAlignment(QtCore.Qt.AlignCenter)
        self.connection_indicator.setObjectName("connection_indicator")
        self.connection_indicator.setStyleSheet(not_connected_style)
        self.connection_indicator.setMaximumHeight(30)

        self.setLayout(vbox)
        vbox.addWidget(self.connection_indicator)
        vbox.addLayout(hbox)
        vbox.addStretch(1)

        self.ports_list = list_ports.comports()
        self.ports_box = QtWidgets.QComboBox()

        self.ports_box.currentIndexChanged.connect(self.selectionChanged)
        self.options_button.clicked.connect(self.options_box.showHide)
        self.sample_button.clicked.connect(self.sample)

        for dev in self.ports_list:
            self.ports_box.addItem(dev.product, userData=dev.device)

        self.options_box.vbox.addWidget(self.ports_box)

        thr = Thread(target=self.check_serial, args=(self.ports_box,))

        thr.daemon = True
        thr.start()

    def sample(self):
        if self.connection.is_open:
            data = self.connection.startSampling()
            data = (self.maskChannel(data, 0))
            self.parent.parent.plot_container.addChannel(data)
        else:
            self.parent.parent.plot_container.addChannel(
                [random.randrange(0, 2, 1) for i in range(0, 100000)])

    def check_serial(self, ports_box_ref):
        while True:
            time.sleep(1)
            new_ports_list = list_ports.comports()
            if sorted(new_ports_list) != sorted(self.ports_list):
                self.connection.close()
                self.ports_list = new_ports_list
                time.sleep(1)
                self.ports_box.clear()
                for dev in self.ports_list:
                    self.ports_box.addItem(dev.product, userData=dev.device)

                self.update()
                self.selectionChanged(self.ports_box.currentIndex())

    def selectionChanged(self, index):
        try:
            if self.ports_box.count():
                print(self.ports_box.itemData(0))
                self.connection.connect_device(self.ports_box.currentData())
        except Exception as e:
            diag = QtWidgets.QMessageBox()
            diag.setText(str(e))
            diag.exec_()
        finally:
            print(self.connection.is_open)
            if self.connection.is_open:
                self.connection_indicator.setText("Connected")
                self.connection_indicator.setStyleSheet(connected_style)
                self.sample_button.setText("Sample")
            else:
                self.connection_indicator.setText("Not Connected")
                self.connection_indicator.setStyleSheet(not_connected_style)
                self.sample_button.setText("Simulate")

    def maskChannel(self, data, ch_no):
        print(data)
        mask = 0x1
        return [((x >> ch_no) & mask) for x in data]
