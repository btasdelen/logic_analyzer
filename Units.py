import math

class Units:
  def __init__(self):
      global si
      si = {
            -12 : {'multiplier' : 10 ** 12, 'prefix' : 'p'},
            -11 : {'multiplier' : 10 ** 12, 'prefix' : 'p'},
            -10 : {'multiplier' : 10 ** 12, 'prefix' : 'p'},
            -9  : {'multiplier' : 10 ** 9, 'prefix' : 'n'},
            -8  : {'multiplier' : 10 ** 9, 'prefix' : 'n'},
            -7  : {'multiplier' : 10 ** 9, 'prefix' : 'n'},
            -6  : {'multiplier' : 10 ** 6, 'prefix' : 'μ'},
            -5  : {'multiplier' : 10 ** 6, 'prefix' : 'μ'},
            -4  : {'multiplier' : 10 ** 6, 'prefix' : 'μ'},
            -3  : {'multiplier' : 10 ** 3, 'prefix' : 'm'},
            -2  : {'multiplier' : 10 ** 3, 'prefix' : 'm'},
            -1  : {'multiplier' : 10 ** 3, 'prefix' : 'm'},
             0  : {'multiplier' : 1, 'prefix' : ''},
             1  : {'multiplier' : 1, 'prefix' : ''},
             2  : {'multiplier' : 1, 'prefix' : ''},
             3  : {'multiplier' : 1, 'prefix' : ''},
             4  : {'multiplier' : 1, 'prefix' : ''},
             5  : {'multiplier' : 1, 'prefix' : ''},
             6  : {'multiplier' : 1, 'prefix' : ''},
            }

  def convert(self, number):
      # Checking if its negative or positive
      if number < 0:
          negative = True;
      else:
          negative = False;

      # if its negative converting to positive (math.log()....)
      if negative:
          number = number - (number*2)

      # Taking the exponent
      exponent = math.floor(math.log10(number))

      # Checking if it was negative converting it back to negative
      if negative:
          number = number - (number*2)

      # If the exponent is smaler than 0 dividing the exponent with -1
      if exponent < 0:
          if math.log10(number) == round(exponent):
            return [number * si[exponent]['multiplier'], si[exponent]['prefix']]
          else:
            exponent = exponent - 1
            return [number * si[exponent]['multiplier'], si[exponent]['prefix']]
      # If the exponent bigger than 0 just return it
      elif exponent > 0:
          return [number / si[exponent]['multiplier'], si[exponent]['prefix']]
      # If the exponent is 0 than return only the value
      elif exponent == 0:
          return [number, '']
